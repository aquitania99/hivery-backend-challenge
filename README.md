# Steps to run the hivery-backend-challenge application containerized

## Tech used on the project

Python 3.7
Django
Django Rest Framework
PostgreSQL
Docker

## Execution

1. `docker build .` then `docker-compose build`

## Django commands

### Migrations - Create/upadte DB schemas

`docker-compose.yml` runs the scripts to do the data population of the db

It executes:

1. `python manage.py makemigrations`
2. `python manage.py migrate`
3. `python manage.py populate_db`
4. `python manage.py loaddata --format=json new_companies.json`
5. `python manage.py loaddata --format=json new_people.json`

### Run unit tests and linting

1. `docker-compose run --rm app sh -c "python manage.py test && flake8"`

### Create Superuser (Admin)

1. `docker-compose run --rm app sh -c "python manage.py createsuperuser"`
   ..\* Fill up the fields (Email & Password)

### Execute Django server to visualize in browser

1. `docker-compose up` Executes `python manage.py runserver 0.0.0.0:8000"`
   ..\* Open browser and go to `http://localhost:8000`
   ..\* To access the admin dashboard go to `http://localhost:8000/admin`

## Docker commands

### List all the active containers

`docker ps`

### List all the containers (active and inactive)

`docker ps -qa`

### Remove all the containers

`docker rm (docker ps -qa)`

### List all the docker images

`docker images ls`

### Removes all images

`docker rmi (docker image ls | awk 'NR>1 {print \$3}')`

### List all the volumes

`docker volume ls`

### Remove all the volumes

`docker volume prune`

## API Endpoints samples

```
### Get list of employees by company id
GET http://localhost:8000/api/employees/?company_id=10

### Get list of people on the database
GET http://localhost:8000/api/people/

### Get person's details by person id
GET http://localhost:8000/api/people/1

### Get friends in common between two persons
GET http://localhost:8000/api/friends/?uid1=2&uid2=15
```
