from django.db import models


class Company(models.Model):
    """Company in Paranuara"""
    index = models.IntegerField(blank=False, null=False, primary_key=True)
    name = models.CharField(max_length=255, blank=False, null=False)

    def __str__(self):
        return self.name


class Person(models.Model):
    """Inhabitant of Paranuara"""
    index = models.IntegerField(
        blank=False, null=False, primary_key=True, auto_created=True)
    guid = models.CharField(max_length=255, blank=True, null=True)
    has_died = models.BooleanField(default=False, null=True)
    balance = models.CharField(max_length=9, blank=True, null=True)
    picture = models.CharField(max_length=255, blank=False, null=True)
    age = models.IntegerField(blank=False, null=True)
    eyeColor = models.CharField(max_length=20, null=True)
    name = models.CharField(max_length=255, null=True)
    gender = models.CharField(max_length=6, null=True)
    company_id = models.IntegerField(blank=False, null=True)
    email = models.CharField(max_length=255)
    phone = models.CharField(max_length=60, blank=True, null=True)
    address = models.CharField(max_length=255, blank=True, null=True)
    about = models.TextField(blank=True, null=True)
    registered = models.DateTimeField(blank=True, null=True)
    tags = models.TextField(blank=True, null=True)
    friends = models.TextField(blank=True, null=True)
    greeting = models.CharField(max_length=255, blank=True, null=True)
    fruits = models.TextField(blank=True, null=True)
    vegetables = models.TextField(blank=True, null=True)

    def __str__(self):

        return self.name
