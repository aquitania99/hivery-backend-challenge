import random
from django.test import TestCase

from core import models


class ModelTests(TestCase):

    def setUp(self):
        pass

    def test_companies_str(self):
        """Test the company string representation"""
        company = models.Company.objects.create(
            index=random.randrange(0, 100),
            name='Test Company'
        )

        self.assertEqual(str(company), company.name)

    def test_people_str(self):
        """Test the Person string representation"""
        person = models.Person.objects.create(
            index=random.randrange(0, 100),
            name='Test Person',
            has_died=False,
            age=35
        )

        self.assertEqual(str(person), person.name)
