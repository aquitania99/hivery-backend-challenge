import os
from datetime import datetime
from pathlib import Path
import json
from django.conf import settings
from django.core.management.base import BaseCommand


class Command(BaseCommand):
    """Django command to populate database from json files in /resources"""

    def handle(self, *args, **options):
        """Handle the command"""
        pwd = os.getcwd()
        self.stdout.write(pwd)
        resources = Path("resources/")
        fixtures = Path("core/fixtures/")
        self.stdout.write('Import data...')

        """Normalize company data to be upload to the DB"""
        companies = resources / 'companies.json'
        companies_fixture = fixtures / 'new_companies.json'
        with open(companies, 'r') as companies_json:
            companies_initial_data = json.load(companies_json)
            """Generate fixture to populate data"""
            company_populate_data = [
                {
                    'model': 'core.company',
                    'pk': i['index'],
                    'fields': {
                        'name': i['company']
                    }
                }
                for i in companies_initial_data
            ]

        with open(companies_fixture, 'w') as new_companies_json:
            json.dump(company_populate_data, new_companies_json)

        self.stdout.write(self.style.SUCCESS('Companies fixture added!'))

        """Normalize People data to be upload to the DB"""
        people = resources / 'people.json'
        people_fixture = fixtures / 'new_people.json'
        fruits_list = settings.FRUITS
        people_populate_data = []

        with open(people, 'r') as new_people_json:
            people_initial_data = json.load(new_people_json)
            for people in people_initial_data:
                fruits = set()
                vegetables = set()
                for f in people['favouriteFood']:
                    if f in fruits_list:
                        fruits.add(f)
                    else:
                        vegetables.add(f)
                friends = [friend['index'] for friend in people['friends']]
                dt = datetime.fromisoformat(people['registered'])
                registered = dt.strftime("%Y-%m-%d %H:%M:%S")
                person = {
                    'model': 'core.person',
                    'pk': people['index'],
                    'fields': {
                        'guid': people['guid'],
                        'has_died': people['has_died'],
                        'balance': people['balance'],
                        'picture': people['picture'],
                        'age': people['age'],
                        'eyeColor': people['eyeColor'],
                        'name': people['name'],
                        'gender': people['gender'],
                        'company_id': people['company_id'],
                        'email': people['email'],
                        'phone': people['phone'],
                        'address': people['address'],
                        'about': people['about'],
                        'registered': registered,
                        'tags': people['tags'],
                        'greeting': people['greeting'],
                        'friends': json.dumps(friends),
                        'fruits': list(fruits) if len(list(fruits)) > 0 else [],
                        'vegetables': list(vegetables) if len(list(vegetables)) > 0 else []
                    }
                }
                people_populate_data.append(person)

        with open(people_fixture, 'w') as new_people_json:
            json.dump(people_populate_data, new_people_json)

        self.stdout.write(self.style.SUCCESS('Food list {0} - {1}\n'.format(fruits, vegetables)))

        self.stdout.write(self.style.SUCCESS('People fixture added!'))
