import json
from rest_framework.renderers import JSONRenderer
from rest_framework import viewsets
from core.models import Company, Person
from django.shortcuts import get_object_or_404

from api import serializers


def index(self):
    pass


class PersonViewSet(viewsets.ReadOnlyModelViewSet):
    """Provides `list` and `detail` actions for People"""
    serializer_class = serializers.PersonSerializer

    def get_queryset(self):
        queryset = Person.objects.all()
        return queryset

    def get_serializer_class(self):
        if self.action == 'retrieve':
            return serializers.PersonDetailSerializer

        return self.serializer_class


class CompanyViewSet(viewsets.ReadOnlyModelViewSet):
    """Provides `list` and `detail` actions for People"""
    serializer_class = serializers.CompanySerializer
    queryset = Company.objects.all()


class EmployeesViewSet(viewsets.ReadOnlyModelViewSet):
    """Return list of Employees for a given company.
    API Endpoint: specify company_id: /?company_id=1"""
    serializer_class = serializers.EmployeeSerializer

    def get_queryset(self):
        param = self.request.query_params.get("company_id", None)
        company = get_object_or_404(Company, pk=int(param))
        if param and company:
            queryset = Person.objects.filter(company_id=int(param))
            if not queryset:
                return {'message': 'Company doesn\'t have any employees'}
            else:
                return queryset.order_by('name')


class PersonFriendsViewSet(viewsets.ModelViewSet):
    """Return list of People and the friends they have in common.
    API endpoint specify Person1 and Person2 Ids: /api/friends/?uid1=3&uid2=2"""

    # serializer_class = serializers.FriendSerializer

    def get_queryset(self):
        param1 = self.request.query_params.get("uid1", None)
        param2 = self.request.query_params.get("uid2", None)

        params_list = [int(param1), int(param2)]

        self.queryset = Person.objects.filter(pk__in=params_list)

        return self.queryset

    def get_serializer_class(self):
        """Return appropiate serializer class"""
        api_response = {'person_details': [], 'common_friends': []}

        friends_list_a = self.queryset[0].friends
        friends_list_b = self.queryset[1].friends

        common_friends_list = self.common_friends(
            friends_list_a, friends_list_b)

        for people in self.queryset:
            api_response['person_details'].append(
                {
                    "name": people.name,
                    "age": people.age,
                    "address": people.address,
                    "phone": people.phone
                }
            )

        api_response['common_friends'].append(common_friends_list)

        obj = serializers.Foo(api_response)

        serializer_class = serializers.FooSerializer(obj)

        print(f'API RESPONSE: {api_response}\n')
        print(f'SERIALIZER DATA: {serializer_class.data}\n')

        return serializer_class.data

    def common_friends(self, list1, list2):
        common_friends = [value for value in list(
            json.loads(list1)) if value in list(json.loads(list2))]
        filtered_friends = Person.objects.filter(pk__in=common_friends,
                                                 eyeColor='brown',
                                                 has_died=False).order_by('-name')
        friends = {}
        if len(filtered_friends) > 0:
            for p in filtered_friends:
                friends['index'] = p.index
                friends['name'] = p.name

        return friends
