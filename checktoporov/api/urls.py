from django.urls import path, include
from rest_framework.routers import DefaultRouter

from . import views

# Create a router and register the viewsets with it
router = DefaultRouter()

router.register(r'employees', views.EmployeesViewSet,
                basename='employees')
router.register(r'people', views.PersonViewSet,
                basename='people')
router.register(r'friends', views.PersonFriendsViewSet,
                basename='friends')

app_name = 'api'

urlpatterns = [
    path('', include(router.urls))
]
