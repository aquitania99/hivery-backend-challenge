from random import randint
from django.urls import reverse
from django.test import TestCase
from rest_framework import status
from rest_framework.test import APIClient

from core.models import Company, Person
from ..serializers import EmployeeSerializer, PersonDetailSerializer


GET_EMPLOYEES_URL = reverse('api:employees-list')
GET_FRIENDS_URL = reverse('api:friends-list')


def get_employees_url(company_id):
    """Return company employees URL"""
    return reverse('api:employees-list', args=[company_id])


def get_friends_url(person1, person2):
    """Return company employees URL"""
    return GET_FRIENDS_URL + f'?uid1={person1}&uid2={person2}'


def get_people_detail_url(person_id):
    """Return people detail URL"""
    return reverse('api:people-detail', args=[person_id])


class PublicUrlApiTests(TestCase):
    """Test the publicly available company API"""

    def setUp(self):

        self.company1 = Company.objects.create(
            index=1, name='Test Company 1'
        )

        self.company2 = Company.objects.create(
            index=2, name='Test Company 2'
        )

        self.person1 = Person.objects.create(
            index=1,
            name='test_user',
            age=54,
            email='test1@email.com',
            address='1 test Street, NSW',
            eyeColor='brown',
            has_died=False,
            fruits=['banana', 'apple'],
            vegetables=['broccoli'],
            friends=[2, 3, 6],
            company_id=1
        )

        self.person2 = Person.objects.create(
            index=2,
            name='test2_user',
            age=43,
            email='test2@email.com',
            address='2 test Street, NSW',
            eyeColor='green',
            has_died=False,
            fruits=['peach', 'pinapple'],
            vegetables=['spinach', 'kale', 'beetroot'],
            friends=[1, 3, 4, 5],
            company_id=1
        )

        self.person3 = Person.objects.create(
            index=3,
            name='test3_user',
            age=34,
            email='test3@email.com',
            address='3 test Street, NSW',
            eyeColor='brown',
            has_died=False,
            fruits=['nectarin', 'pear', 'strawberry'],
            vegetables=['tomato', 'avocado', 'lettuce'],
            friends=[1, 2, 5],
            company_id=2
        )

        self.person4 = Person.objects.create(
            index=4,
            name='test_user',
            age=60,
            email='test4@email.com',
            address='4 test Street, NSW',
            eyeColor='brown',
            has_died=True,
            fruits=['banana', 'apple'],
            vegetables=['broccoli'],
            friends=[1, 2, 3, 6],
            company_id=3
        )

        self.person5 = Person.objects.create(
            index=5,
            name='test_user',
            age=30,
            email='test5@email.com',
            address='5 test Street, NSW',
            eyeColor='blue',
            has_died=False,
            fruits=['coconut', 'orange'],
            vegetables=['broccoli, avocado'],
            friends=[1, 2, 3, 4, 6],
            company_id=2
        )

        self.person6 = Person.objects.create(
            index=6,
            name='test_user',
            age=67,
            email='test6@email.com',
            address='6 test Street, NSW',
            eyeColor='brown',
            has_died=False,
            fruits=['coconut', 'orange'],
            vegetables=['broccoli, avocado'],
            friends=[1, 2, 3, 4],
            company_id=2
        )

        self.rest_client = APIClient()

        self.people = {1: self.person1, 2: self.person2, 3: self.person3}

    def test_get_employees_list(self):
        """Test getting the list of Employees for a given Company"""
        res = self.rest_client.get(
            GET_EMPLOYEES_URL + f'?company_id={self.company1.index}')
        employees = Person.objects.filter(
            company_id=self.company1.index
        ).order_by('name')
        serializer = EmployeeSerializer(employees, many=True)
        self.assertEqual(res.status_code, status.HTTP_200_OK)
        self.assertEqual(len(res.data), 2)
        self.assertEqual(res.data, serializer.data)

    def test_get_person_details(self):
        """Test getting the data of a person by ID"""
        random_id = randint(1, 3)
        url = get_people_detail_url(random_id)
        res = self.rest_client.get(url)

        serializer = PersonDetailSerializer(self.people[random_id])

        self.assertEqual(res.status_code, status.HTTP_200_OK)
        self.assertEqual(res.data, serializer.data)

    def test_get_common_friends(self):
        """Test getting frinds in common between 2 people"""
        from random import choice

        p1_id = randint(1, 6)
        p2_id = choice([i for i in range(1, 6) if i not in [p1_id]])

        url = get_friends_url(p1_id, p2_id)
        res = self.rest_client.get(url)

        self.assertEqual(res.status_code, status.HTTP_200_OK)
