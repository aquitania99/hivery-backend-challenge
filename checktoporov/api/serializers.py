from rest_framework import serializers
from core.models import Company, Person


class PersonSerializer(serializers.ModelSerializer):
    """Serializer for Person objects"""

    class Meta:
        model = Person
        fields = (
            'has_died',
            'age',
            'eyeColor',
            'name',
            'gender',
            'company_id',
            'email',
            'phone',
            'address',
            'tags',
            'friends',
            'fruits',
            'vegetables'
        )


class CompanySerializer(serializers.ModelSerializer):
    """Serializer for Company objects"""
    class Meta:
        model = Company
        fields = ('index', 'name')


class EmployeeSerializer(serializers.ModelSerializer):
    """Serializer for Employee objects"""
    class Meta:
        model = Person
        fields = ('name', 'email', 'company_id')


class PersonDetailSerializer(serializers.ModelSerializer):
    """Serialize person details"""
    class Meta:
        model = Person
        fields = (
            'name',
            'age',
            'fruits',
            'vegetables'
        )


class FriendSerializer(serializers.ModelSerializer):
    """Serialize persons with friends in common"""

    class Meta:
        model = Person
        fields = (
            'name',
            'age',
            'address',
            'phone'
        )


class Foo(object):
    def __init__(self, json_data):
        self.person_details = json_data['person_details']
        self.common_friends = json_data['common_friends']


class FooSerializer(serializers.Serializer):
    # initialize fields
    person_details = serializers.JSONField()
    common_friends = serializers.JSONField()
